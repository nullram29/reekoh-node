/* global describe, it, before, after */

'use strict'

let _conn = null
let _plugin = null
const amqp = require('amqplib')

const reekoh = require('../../index.js')
const isEqual = require('lodash.isequal')

const ACCOUNT = 'demo.account'
const PLUGIN_ID = 'demo.gateway'
const COMMAND_RELAYS = 'demo.relay1'
const OUTPUT_PIPES = 'demo.outpipe.1,demo.outpipe.2'
const BROKER = 'amqp://guest:guest@127.0.0.1/reekoh'

describe('Gateway Plugin Test', () => {
  before('#test init', () => {
    process.env.LOGGERS = ''
    process.env.CONFIG = '{}'
    process.env.EXCEPTION_LOGGERS = ''

    process.env.BROKER = BROKER
    process.env.ACCOUNT = ACCOUNT
    process.env.PLUGIN_ID = PLUGIN_ID
    process.env.COMMAND_RELAYS = COMMAND_RELAYS
    process.env.OUTPUT_PIPES = OUTPUT_PIPES

    amqp.connect(process.env.BROKER).then((conn) => {
      _conn = conn
      return conn.createChannel()
    }).catch((err) => {
      console.log(err)
    })
  })

  after('terminate connection', () => {
    _conn.close()
  })

  describe('#spawn', () => {
    it('should spawn the class without error', (done) => {
      try {
        _plugin = new reekoh.plugins.Gateway()
        done()
      } catch (err) {
        done(err)
      }
    })
  })

  describe('#events', () => {
    it('should rcv `ready` event', (done) => {
      _plugin.once('ready', () => {
        done()
      })
    })

    it('should rcv `command` event', function (done) {
      this.timeout(10000)

      _plugin.on('command', () => {
        done()
      })
      _plugin.relayCommand('ACTIVATE', '567827489028375', '', '567827489028376')
    })
  })

  describe('#pipe()', () => {
    it('should throw error if data is empty', (done) => {
      _plugin.pipe('', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Invalid data received. Data should be and Object and should not empty.')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    let data = {
      foo: 'bar',
      rkhDeviceInfo: {
        account: ACCOUNT,
        _id: '567827489028375'
      }
    }

    it('should publish data to output pipes', (done) => { // no sequence id
      _plugin.pipe(data, '').then(() => {
        done()
      }).catch(done)
    })

    it('should publish data to sanitizer', (done) => {
      _plugin.pipe(data, 'seq123').then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#relayCommand()', () => {
    it('should throw error if message is empty', (done) => {
      _plugin.relayCommand('', '', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify the command/message to send')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if device or deviceTypes is empty', (done) => {
      _plugin.relayCommand('test', '', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify the target device types or devices')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish a message to `Message Relay Queue`', (done) => {
      _plugin.relayCommand('test', ['a'], ['b']).then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#sendCommandResponse()', () => {
    it('should throw error if commandId is empty', (done) => {
      _plugin.sendCommandResponse('', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify the command id')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if response is empty', (done) => {
      _plugin.sendCommandResponse('foo', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify the response')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish a message to `cmd.response` queue', (done) => {
      _plugin.sendCommandResponse('foo', 'bar').then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#notifyConnection()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.notifyConnection('').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.notifyConnection(123).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish a message to device', function (done) {
      this.timeout(11000)

      _plugin.notifyConnection('567827489028375').then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#notifyDisconnection()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.notifyDisconnection('').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.notifyDisconnection(123).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish a message to device', (done) => {
      _plugin.notifyDisconnection('test').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#syncDevice()', () => {
    it('should throw error if deviceInfo is empty', (done) => {
      _plugin.syncDevice('', []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device information/details')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo is not a valid object', (done) => {
      _plugin.syncDevice('{}', []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device information/details')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo doesnt have `_id` or `id` property', (done) => {
      _plugin.syncDevice({}, []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid id for the device')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceInfo doesnt have `name` property', (done) => {
      _plugin.syncDevice({ _id: '123' }, []).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid name for the device')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish sync msg to queue', (done) => {
      _plugin.syncDevice({ _id: '123', name: 'foo' }, []).then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#requestDeviceInfo()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.requestDeviceInfo('', () => {}).then(() => {
        // noop!
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.requestDeviceInfo('', () => {}).then(() => {
        // noop!
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should request device info', function (done) {
      this.timeout(3000)

      _plugin.requestDeviceInfo('567827489028375').then(() => {
        done()
      }).catch((err) => {
        done(err)
      })
    })
  })

  describe('#removeDevice()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.removeDevice('').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.removeDevice(123).then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish remove msg to queue', (done) => {
      _plugin.removeDevice('123').then(() => {
        setTimeout(done, 500)
      }).catch(done)
    })
  })

  describe('#setDeviceState()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.setDeviceState('', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.setDeviceState(123, '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if state is empty', (done) => {
      _plugin.setDeviceState('test').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify the device state')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should publish state msg to queue', (done) => {
      _plugin.setDeviceState('567827489028375', 'bar').then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#setDeviceLocation()', () => {
    it('should throw error if deviceId is empty', (done) => {
      _plugin.setDeviceLocation('', '', '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if deviceId is not a string', (done) => {
      _plugin.setDeviceLocation(123, '').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, 'Kindly specify a valid device identifier')) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if lat is not valid', (done) => {
      _plugin.setDeviceLocation('test', 'foo').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, `Kindly specify a valid 'lat, long' coordinates`)) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it('should throw error if long is not valid', (done) => {
      _plugin.setDeviceLocation('test', 1.23232, 'bar').then(() => {
        done(new Error('Expecting rejection. check function test param.'))
      }).catch((err) => {
        if (!isEqual(err.message, `Kindly specify a valid 'lat, long' coordinates`)) {
          done(new Error('Returned value not matched.'))
        } else {
          done()
        }
      })
    })

    it(`should publish 'device.location' msg to queue`, (done) => {
      _plugin.setDeviceLocation('567827489028375', 14.5831, 120.9794).then(() => {
        done()
      }).catch(done)
    })
  })

  describe('#logging', () => {
    it('should send a log to logger queues', (done) => {
      _plugin.log('dummy log data').then(() => {
        done()
      }).catch(done)
    })

    it('should send an exception log to exception logger queues', (done) => {
      _plugin.logException(new Error('test err msg')).then(() => {
        done()
      }).catch(done)
    })
  })
})
