'use strict'

const path = require('path')
const uuid = require('uuid/v4')
const BPromise = require('bluebird')
const safeParse = BPromise.method(JSON.parse)
const fs = BPromise.promisifyAll(require('fs'))

const isNil = require('lodash.isnil')
const isEmpty = require('lodash.isempty')
const isError = require('lodash.iserror')
const isString = require('lodash.isstring')
const isPlainObject = require('lodash.isplainobject')

const Prom = require('../prometheus-logger')
const LatencyMonitor = require('../prometheus-latency')
const Broker = require('../broker.lib')

class Channel extends LatencyMonitor {
  constructor () {
    super()
    this.config = {}
    this.port = process.env.PORT || 8080

    const BROKER = process.env.BROKER
    const ACCOUNT = process.env.ACCOUNT
    const INPUT_PIPE = process.env.INPUT_PIPE

    const LOGGERS = `${process.env.LOGGERS || ''}`.split(',').filter(Boolean)
    const COMMAND_RELAYS = `${process.env.COMMAND_RELAYS || ''}`.split(',').filter(Boolean)
    const EXCEPTION_LOGGERS = `${process.env.EXCEPTION_LOGGERS || ''}`.split(',').filter(Boolean)

    let _broker = new Broker()
    let _prometheus = new Prom()

    process.env.PORT = undefined
    process.env.BROKER = undefined
    process.env.ACCOUNT = undefined
    process.env.LOGGERS = undefined
    process.env.PIPELINE = undefined
    process.env.INPUT_PIPE = undefined
    process.env.COMMAND_RELAYS = undefined
    process.env.EXCEPTION_LOGGERS = undefined

    this.relayCommand = (command, devices, deviceGroups, source = '') => {
      if (!command) return BPromise.reject(new Error('Kindly specify the command/message to send'))
      if (!isString(command)) return BPromise.reject(new Error('Command must be a valid string'))
      if (isEmpty(devices) && isEmpty(deviceGroups)) return BPromise.reject(new Error('Kindly specify the target device types or devices'))

      if (!(isString(deviceGroups) || Array.isArray(deviceGroups)) || !(isString(devices) || Array.isArray(devices))) {
        return BPromise.reject(new Error(`'deviceGroups' and 'devices' must be a string or an array.`))
      }

      let sequenceId = uuid()

      return BPromise.each(COMMAND_RELAYS, cmdRelay => {
        return _broker.queues[cmdRelay].publish({
          account: ACCOUNT,
          pluginId: INPUT_PIPE,
          command: command,
          sequenceId: sequenceId,
          deviceGroups: deviceGroups,
          devices: devices,
          source: source
        })
      })
    }

    this.setState = (state) => {
      if (isNil(state) || (isString(state) && isEmpty(state))) {
        return BPromise.reject(new Error(`Please specify a valid state to set.`))
      }

      return _broker.queues['plugin.state'].publish({
        state: state,
        plugin: INPUT_PIPE
      })
    }

    this.getState = () => {
      return new BPromise((resolve, reject) => {
        let requestId = uuid()
        _broker.rpcs['plugin.state.rpc'].once(requestId, (state) => {
          resolve(state.content.toString())
        })
        _broker.rpcs['plugin.state.rpc'].publish(requestId, {
          plugin: INPUT_PIPE
        }).catch(reject)
      }).timeout(10000, 'Request for plugin state has timed out.')
    }

    this.log = (logData) => {
      if (isEmpty(logData)) return BPromise.reject(new Error(`Please specify a data to log.`))
      if (!isPlainObject(logData) && !isString(logData)) return BPromise.reject(new Error('Log data must be a string or object'))

      return BPromise.all([
        BPromise.each(LOGGERS, logger => {
          return _broker.queues[logger].publish(logData)
        }),
        _broker.queues['logs'].publish({
          account: ACCOUNT,
          pluginId: INPUT_PIPE,
          type: 'Channel',
          data: logData
        })
      ])
    }

    this.logException = (err) => {
      _prometheus.error()
      if (!isError(err)) return BPromise.reject(new Error('Please specify a valid error to log.'))

      let errData = {
        name: err.name,
        message: err.message,
        stack: err.stack
      }

      return BPromise.all([
        BPromise.each(EXCEPTION_LOGGERS, exceptionLogger => {
          return _broker.queues[exceptionLogger].publish(errData)
        }),
        _broker.queues['exceptions'].publish({
          account: ACCOUNT,
          pluginId: INPUT_PIPE,
          type: 'Channel',
          data: errData
        })
      ])
    }

    safeParse(process.env.CONFIG || '{}').then(config => {
      this.config = config
      process.env.CONFIG = undefined

      return BPromise.resolve()
    }).then(() => {
      return _broker.connect(BROKER)
    }).then(() => {
      let root = process.cwd()
      let home = path.join(root, 'keys')

      if (!fs.existsSync(home)) fs.mkdirSync(home)

      let writeFile = (filePath, content) => {
        if (isNil(content) || isEmpty(content)) return BPromise.resolve('')

        return fs.writeFileAsync(filePath, content).then(() => {
          return BPromise.resolve(filePath)
        })
      }

      return BPromise.props({
        rootCrl: writeFile(path.join(home, 'root-crl.pem'), process.env.ROOT_CRL),
        ca: writeFile(path.join(home, 'server-ca.pem'), process.env.CA),
        crl: writeFile(path.join(home, 'server-crl.pem'), process.env.CRL),
        key: writeFile(path.join(home, 'server-key.pem'), process.env.KEY),
        cert: writeFile(path.join(home, 'server-cert.pem'), process.env.CERT)
      }).then(keys => {
        Object.assign(this, keys)

        process.env.ROOT_CRL = undefined
        process.env.CA = undefined
        process.env.CRL = undefined
        process.env.KEY = undefined
        process.env.CERT = undefined

        return BPromise.resolve()
      })
    }).then(() => {
      let queueIds = [INPUT_PIPE, 'logs', 'exceptions', 'plugin.state']
        .concat(EXCEPTION_LOGGERS)
        .concat(COMMAND_RELAYS)
        .concat(LOGGERS)

      return BPromise.each(queueIds, queueId => {
        return _broker.createQueue(queueId)
      })
    }).then(() => {
      return _broker.createExchange(`${INPUT_PIPE}.topic`).then((exchange) => {
        return exchange.consume((msg) => {
          safeParse(msg.content.toString() || '{}').then(data => {
            this.emit('data', data)
          }).catch(err => {
            console.error(err)
          })
        })
      })
    }).then(() => {
      return _broker.queues[INPUT_PIPE].consume((msg) => {
        _broker.exchanges[`${INPUT_PIPE}.topic`].publish(msg.content.toString())
      })
    }).then(() => {
      return _broker.createRPC('client', 'plugin.state.rpc').then((queue) => {
        return queue.consume()
      })
    }).then(() => {
      process.nextTick(() => {
        require('../../http-server/prom-http-server')
        this.emit('ready')
      })

      return BPromise.resolve()
    }).catch(err => {
      console.error(err)
      throw err
    })
  }
}

module.exports = Channel
