'use strict'

const uuid = require('uuid/v4')
const BPromise = require('bluebird')
const safeParse = BPromise.method(JSON.parse)

const hasProp = require('lodash.has')
const isNil = require('lodash.isnil')
const isError = require('lodash.iserror')
const isEmpty = require('lodash.isempty')
const isString = require('lodash.isstring')
const isPlainObject = require('lodash.isplainobject')

const Prom = require('../prometheus-logger')
const LatencyMonitor = require('../prometheus-latency')
const Broker = require('../broker.lib')

class InventorySync extends LatencyMonitor {
  constructor () {
    super()
    this.config = {}

    const BROKER = process.env.BROKER
    const ACCOUNT = process.env.ACCOUNT
    const PLUGIN_ID = process.env.PLUGIN_ID

    const LOGGERS = `${process.env.LOGGERS || ''}`.split(',').filter(Boolean)
    const EXCEPTION_LOGGERS = `${process.env.EXCEPTION_LOGGERS || ''}`.split(',').filter(Boolean)

    let _broker = new Broker()
    let _prometheus = new Prom()

    process.env.BROKER = undefined
    process.env.ACCOUNT = undefined
    process.env.LOGGERS = undefined
    process.env.PLUGIN_ID = undefined
    process.env.EXCEPTION_LOGGERS = undefined

    this.syncDevice = (deviceInfo, deviceGroup) => {
      if (isEmpty(deviceInfo)) {
        return BPromise.reject(new Error('Kindly specify the device information/details'))
      }

      if (!isPlainObject(deviceInfo)) {
        return BPromise.reject(new Error('Device info must be an object'))
      }

      if (!(hasProp(deviceInfo, '_id') || hasProp(deviceInfo, 'id'))) {
        return BPromise.reject(new Error('Kindly specify a valid id for the device'))
      }

      if (!hasProp(deviceInfo, 'name')) {
        return BPromise.reject(new Error('Kindly specify a valid name for the device'))
      }

      return _broker.queues['devices'].publish({
        operation: 'sync',
        account: ACCOUNT,
        data: {
          group: deviceGroup,
          device: deviceInfo
        }
      })
    }

    this.removeDevice = (deviceId) => {
      if (!deviceId || !isString(deviceId)) {
        return BPromise.reject(new Error('Kindly specify a valid device identifier'))
      }

      return _broker.queues['devices'].publish({
        operation: 'remove',
        account: ACCOUNT,
        device: {
          _id: deviceId
        }
      })
    }

    this.setState = (state) => {
      if (isNil(state) || (isString(state) && isEmpty(state))) {
        return BPromise.reject(new Error(`Please specify a valid state to set.`))
      }

      return _broker.queues['plugin.state'].publish({
        state: state,
        plugin: PLUGIN_ID
      })
    }

    this.getState = () => {
      return new BPromise((resolve, reject) => {
        let requestId = uuid()
        _broker.rpcs['plugin.state.rpc'].once(requestId, (state) => {
          resolve(state.content.toString())
        })
        _broker.rpcs['plugin.state.rpc'].publish(requestId, {
          plugin: PLUGIN_ID
        }).catch(reject)
      }).timeout(10000, 'Request for plugin state has timed out.')
    }

    this.log = (logData) => {
      if (isEmpty(logData)) return BPromise.reject(new Error(`Please specify a data to log.`))
      if (!isPlainObject(logData) && !isString(logData)) return BPromise.reject(new Error('Log data must be a string or object'))

      return BPromise.all([
        BPromise.each(LOGGERS, logger => {
          return _broker.queues[logger].publish(logData)
        }),
        _broker.queues['logs'].publish({
          account: ACCOUNT,
          pluginId: PLUGIN_ID,
          type: 'Inventory Sync',
          data: logData
        })
      ])
    }

    this.logException = (err) => {
      _prometheus.error()
      if (!isError(err)) return BPromise.reject(new Error('Please specify a valid error to log.'))

      let errData = {
        name: err.name,
        message: err.message,
        stack: err.stack
      }

      return BPromise.all([
        BPromise.each(EXCEPTION_LOGGERS, exceptionLogger => {
          return _broker.queues[exceptionLogger].publish(errData)
        }),
        _broker.queues['exceptions'].publish({
          account: ACCOUNT,
          pluginId: PLUGIN_ID,
          type: 'Inventory Sync',
          data: errData
        })
      ])
    }

    safeParse(process.env.CONFIG || '{}').then(config => {
      this.config = config
      process.env.CONFIG = undefined

      return BPromise.resolve()
    }).then(() => {
      return _broker.connect(BROKER)
    }).then(() => {
      let queueIds = [PLUGIN_ID, 'devices', 'plugin.state', 'logs', 'exceptions']
        .concat(EXCEPTION_LOGGERS)
        .concat(LOGGERS)

      return BPromise.each(queueIds, queueId => {
        return _broker.createQueue(queueId)
      })
    }).then(() => {
      return _broker.queues[PLUGIN_ID].consume((msg) => {
        safeParse(msg.content.toString() || '{}').then(task => {
          switch (task.operation) {
            case 'sync':
              this.emit('sync')
              break
            case 'adddevice':
              this.emit('adddevice', task.device)
              break
            case 'updatedevice':
              this.emit('updatedevice', task.device)
              break
            case 'removedevice':
              this.emit('removedevice', task.device)
              break
          }
        })
      })
    }).then(() => {
      return _broker.createRPC('client', 'plugin.state.rpc').then((queue) => {
        return queue.consume()
      })
    }).then(() => {
      process.nextTick(() => {
        require('../../http-server/prom-http-server')
        this.emit('ready')
      })

      return BPromise.resolve()
    }).catch(err => {
      console.error(err)
      throw err
    })
  }
}

module.exports = InventorySync
