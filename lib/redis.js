'use strict'

let _ = require('lodash')
let Redis = require('ioredis')
let pino = require('pino')

class Cache {
  constructor () {
    let logStream = {
      write: function (chunk) {
        console.log(chunk)
      }
    }

    let pretty = pino.pretty({
      levelFirst: true,
      forceColor: true
    })

    pretty.pipe(process.stdout)

    let logger = pino({
      name: process.env.API_NAME,
      safe: true
    }, (process.env.NODE_ENV === 'production') ? logStream : pretty)
    this.logger = logger
  }

  bootstrapSingleNode () {
    return new Promise((resolve, reject) => {
      this.logger.info('Connecting to Cache Server...')

      let options = {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        password: process.env.REDIS_PASSWORD,
        dropBufferSupport: true,
        keepAlive: 10000,
        retryStrategy: function () {
          return 3000
        },
        reconnectOnError: function () {
          return true
        }
      }

      if (process.env.REDIS_SECURE === 'true') {
        Object.assign(options, {
          tls: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            servername: process.env.REDIS_HOST
          }
        })
      }

      let redis = new Redis(options)

      redis.on('error', (err) => {
        if (process.env.NODE_ENV !== 'development') {
          this.logger.error(err)
          reject(err)
        }
      })

      redis.on('close', () => {
        this.logger.info('Cache client has disconnected.')
      })

      redis.on('end', () => {
        this.logger.info('Cache connection has ended.')
      })

      redis.on('reconnecting', () => {
        this.logger.info('Reconnecting to Cache Server...')
      })

      redis.on('connect', () => {
        this.logger.info('Connected to Cache Server.')
      })

      redis.once('ready', () => {
        this.logger.info('Cache Server ready.')

        resolve(redis)
      })

      this.cache = redis
    })
  }

  bootstrapCluster () {
    return new Promise((resolve, reject) => {
      this.logger.info('Connecting to Cache Cluster...')

      let hosts = `${process.env.REDIS_HOST || ''}`.split(',').filter(Boolean)
      let ports = `${process.env.REDIS_PORT || ''}`.split(',').filter(Boolean)
      let clusterNodes = []

      for (let i = 0; i < hosts.length; i++) {
        if (process.env.REDIS_SECURE === 'true') {
          clusterNodes.push({
            host: hosts[i],
            port: ports[i],
            password: process.env.REDIS_PASSWORD,
            tls: {
              host: hosts[i],
              port: ports[i]
            }
          })
        } else {
          clusterNodes.push({
            host: hosts[i],
            port: ports[i],
            password: process.env.REDIS_PASSWORD
          })
        }
      }

      let options = {
        password: process.env.REDIS_PASSWORD,
        dropBufferSupport: true,
        keepAlive: 10000,
        retryStrategy: function () {
          return 3000
        },
        reconnectOnError: function () {
          return true
        }
      }

      let cluster = new Redis.Cluster(clusterNodes, {
        redisOptions: options
      })

      cluster.on('error', (err) => {
        if (process.env.NODE_ENV !== 'development') {
          this.logger.error(err)
          reject(err)
        }
      })

      cluster.on('close', () => {
        this.logger.info('Cache client has disconnected.')
      })

      cluster.on('end', () => {
        this.logger.info('Cache connection has ended.')
      })

      cluster.on('reconnecting', () => {
        this.logger.info('Reconnecting to Cache Cluster...')
      })

      cluster.on('connect', () => {
        this.logger.info('Connected to Cache Cluster.')
      })

      cluster.once('ready', () => {
        this.logger.info('Cache Cluster ready.')

        resolve(cluster)
      })

      this.cache = cluster
    })
  }

  bootstrap () {
    let hosts = `${process.env.REDIS_HOST || ''}`.split(',').filter(Boolean)

    if (Array.isArray(hosts) && hosts.length > 1) {
      return this.bootstrapCluster()
    } else {
      return this.bootstrapSingleNode()
    }
  }

  close () {
    this.logger.info('Closing Cache Server connection.')

    return new Promise((resolve, reject) => {
      this.cache.quit((err) => {
        if (!_.isNil(err)) {
          if (process.env.NODE_ENV !== 'development') {
            reject(err)
          }
        } else {
          this.logger.info('Cache Server connection closed.')

          resolve()
        }
      })
    })
  }
}

module.exports = Cache
